package de.qwerty287.trianglecalculator

import android.widget.TextView
import java.lang.Exception

class Parser(var triangle: Triangle) {

    fun set(triangle: Triangle) {
        this.triangle = triangle
    }

    private fun check(): Boolean {
        if (triangle.countEdges() < 1) {
            return false
        }

        if (triangle.countEdges() + triangle.countAngles() < 3) { // we need at least three arguments
            return false
        }

        if ((triangle.alpha != null && triangle.beta != null && triangle.gamma != null) &&
            (triangle.alpha!! + triangle.beta!! + triangle.gamma!!) != 180.0) {
            return false
        }

        return true
    }

    fun parse(tv: TextView) {
        if (!check()) {
            return tv.setText(R.string.not_enough_args)
        }

        try {
            tv.text = triangle.calcRemainingParts().createResStr(tv.context)
        } catch (e: Exception) {
            tv.text = e.stackTraceToString()
        }
    }

}