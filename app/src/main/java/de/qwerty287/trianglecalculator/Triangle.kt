package de.qwerty287.trianglecalculator

import android.content.Context
import kotlin.math.*

data class Triangle(var a: Double?, var b: Double?, var c: Double?, var alpha: Double?, var beta: Double?, var gamma: Double?) {

    private enum class Mode {
        EEE,
        EEA,
        EAE,
        AAE,
        AEA
    }
    private var mode: Mode? = null

    fun calcRemainingParts(): Triangle {
        val triangle = Triangle(a, b, c, alpha, beta, gamma)

        // try calculating angles
        if (triangle.alpha == null && triangle.beta != null && triangle.gamma != null) {
            triangle.alpha = 180 - triangle.beta!! - triangle.gamma!!
        }
        if (triangle.beta == null && triangle.alpha != null && triangle.gamma != null) {
            triangle.beta = 180 - triangle.alpha!! - triangle.gamma!!
        }
        if (triangle.gamma == null && triangle.alpha != null && triangle.beta != null) {
            triangle.gamma = 180 - triangle.alpha!! - triangle.beta!!
        }

        mode = when {
            countEdges() == 3 -> {
                Mode.EEE
            }
            countEdges() == 2 -> {
                when {
                    a == null -> {
                        if (alpha != null) {
                            Mode.EAE
                        } else {
                            Mode.EEA
                        }
                    }
                    b == null -> {
                        if (beta != null) {
                            Mode.EAE
                        } else {
                            Mode.EEA
                        }
                    }
                    c == null -> {
                        if (gamma != null) {
                            Mode.EAE
                        } else {
                            Mode.EEA
                        }
                    }
                    else -> return triangle
                }
            }
            countEdges() == 1 -> {
                when {
                    a != null -> {
                        if (alpha != null) {
                            Mode.AAE
                        } else {
                            Mode.AEA
                        }
                    }
                    b != null -> {
                        if (beta != null) {
                            Mode.AAE
                        } else {
                            Mode.AEA
                        }
                    }
                    c != null -> {
                        if (gamma != null) {
                            Mode.AAE
                        } else {
                            Mode.AEA
                        }
                    }
                    else -> return triangle
                }

            }
            else -> return triangle
        }

        triangle.mode = mode

        when (mode) {
            Mode.EEE -> {
                triangle.alpha = Math.toDegrees(acos((triangle.a!!.pow(2) -
                            triangle.b!!.pow(2) -
                            triangle.c!!.pow(2)) / (-2 * triangle.b!! * triangle.c!!)))

                triangle.beta = Math.toDegrees(acos((triangle.b!!.pow(2) -
                        triangle.a!!.pow(2) -
                        triangle.c!!.pow(2)) / (-2 * triangle.a!! * triangle.c!!)))

                triangle.gamma = 180 - triangle.alpha!! - triangle.beta!!
            }
            Mode.EEA -> {
                if (triangle.alpha != null) {
                    if (triangle.b != null) {
                        triangle.beta =
                            Math.toDegrees(asin(sin(Math.toRadians(triangle.alpha!!)) / triangle.a!! * triangle.b!!))
                        triangle.gamma = 180 - triangle.alpha!! - triangle.beta!!
                        triangle.c = triangle.a!! / sin(Math.toRadians(triangle.alpha!!)) * sin(
                            Math.toRadians(triangle.gamma!!)
                        )
                    } else if (triangle.c != null) {
                        triangle.gamma =
                            Math.toDegrees(asin(sin(Math.toRadians(triangle.alpha!!)) / triangle.a!! * triangle.c!!))
                        triangle.beta = 180 - triangle.alpha!! - triangle.gamma!!
                        triangle.b = triangle.a!! / sin(Math.toRadians(triangle.alpha!!)) * sin(
                            Math.toRadians(triangle.beta!!)
                        )
                    }
                } else if (triangle.beta != null) {
                    if (triangle.a != null) {
                        triangle.alpha =
                            Math.toDegrees(asin(sin(Math.toRadians(triangle.beta!!)) / triangle.b!! * triangle.a!!))
                        triangle.gamma = 180 - triangle.alpha!! - triangle.beta!!
                        triangle.c = triangle.a!! / sin(Math.toRadians(triangle.alpha!!)) * sin(
                            Math.toRadians(triangle.gamma!!)
                        )
                    } else if (triangle.c != null) {
                        triangle.gamma =
                            Math.toDegrees(asin(sin(Math.toRadians(triangle.beta!!)) / triangle.b!! * triangle.c!!))
                        triangle.alpha = 180 - triangle.beta!! - triangle.gamma!!
                        triangle.a = triangle.c!! / sin(Math.toRadians(triangle.beta!!)) * sin(
                            Math.toRadians(triangle.alpha!!)
                        )
                    }
                } else if (triangle.gamma != null) {
                    if (triangle.a != null) {
                        triangle.alpha =
                            Math.toDegrees(asin(sin(Math.toRadians(triangle.gamma!!)) / triangle.c!! * triangle.a!!))
                        triangle.beta = 180 - triangle.alpha!! - triangle.gamma!!
                        triangle.b = triangle.a!! / sin(Math.toRadians(triangle.alpha!!)) * sin(
                            Math.toRadians(triangle.beta!!)
                        )
                    } else if (triangle.b != null) {
                        triangle.beta =
                            Math.toDegrees(asin(sin(Math.toRadians(triangle.gamma!!)) / triangle.c!! * triangle.b!!))
                        triangle.alpha = 180 - triangle.beta!! - triangle.gamma!!
                        triangle.a = triangle.b!! / sin(Math.toRadians(triangle.beta!!)) * sin(
                            Math.toRadians(triangle.alpha!!)
                        )
                    }
                }
            }
            Mode.AAE -> {
                when {
                    triangle.a != null -> {
                        triangle.b = triangle.a!! / sin(Math.toRadians(triangle.alpha!!)) * sin(Math.toRadians(triangle.beta!!))
                        triangle.c = triangle.a!! / sin(Math.toRadians(triangle.alpha!!)) * sin(Math.toRadians(triangle.gamma!!))
                    }
                    triangle.b != null -> {
                        triangle.a = triangle.b!! / sin(Math.toRadians(triangle.beta!!)) * sin(Math.toRadians(triangle.alpha!!))
                        triangle.c = triangle.b!! / sin(Math.toRadians(triangle.beta!!)) * sin(Math.toRadians(triangle.gamma!!))
                    }
                    triangle.c != null -> {
                        triangle.a = triangle.c!! / sin(Math.toRadians(triangle.gamma!!)) * sin(Math.toRadians(triangle.alpha!!))
                        triangle.b = triangle.c!! / sin(Math.toRadians(triangle.gamma!!)) * sin(Math.toRadians(triangle.beta!!))
                    }
                }
            }
            Mode.AEA -> {
                when {
                    triangle.a != null -> {
                        triangle.b = triangle.a!! / sin(Math.toRadians(triangle.alpha!!)) * sin(
                            Math.toRadians(triangle.beta!!)
                        )
                        triangle.c = triangle.a!! / sin(Math.toRadians(triangle.alpha!!)) * sin(
                            Math.toRadians(triangle.gamma!!)
                        )
                    }
                    triangle.b != null -> {
                        triangle.a = triangle.b!! / sin(Math.toRadians(triangle.beta!!)) * sin(
                            Math.toRadians(triangle.alpha!!)
                        )
                        triangle.c = triangle.b!! / sin(Math.toRadians(triangle.beta!!)) * sin(
                            Math.toRadians(triangle.gamma!!)
                        )
                    }
                    triangle.c != null -> {
                        triangle.a = triangle.c!! / sin(Math.toRadians(triangle.gamma!!)) * sin(
                            Math.toRadians(triangle.alpha!!)
                        )
                        triangle.b = triangle.c!! / sin(Math.toRadians(triangle.gamma!!)) * sin(
                            Math.toRadians(triangle.beta!!)
                        )
                    }
                }
            }
            Mode.EAE -> {
                when {
                    triangle.alpha != null -> {
                        triangle.a = sqrt(triangle.b!!.pow(2) + triangle.c!!.pow(2) -
                                2 * triangle.b!! * triangle.c!! * cos(Math.toRadians(triangle.alpha!!)))
                        triangle.beta = Math.toDegrees((acos((triangle.b!!.pow(2) -
                                triangle.a!!.pow(2) -
                                triangle.c!!.pow(2)) / (-2 * triangle.a!! * triangle.c!!))))
                        triangle.gamma = 180 - triangle.alpha!! - triangle.beta!!
                    }
                    triangle.beta != null -> {
                        triangle.b = sqrt(triangle.a!!.pow(2) + triangle.c!!.pow(2) -
                                2 * triangle.a!! * triangle.c!! * cos(Math.toRadians(triangle.beta!!)))
                        triangle.alpha = Math.toDegrees((acos((triangle.a!!.pow(2) -
                                triangle.b!!.pow(2) -
                                triangle.c!!.pow(2)) / (-2 * triangle.b!! * triangle.c!!))))
                        triangle.gamma = 180 - triangle.alpha!! - triangle.beta!!
                    }
                    triangle.gamma != null -> {
                        triangle.c = sqrt(triangle.a!!.pow(2) + triangle.b!!.pow(2) -
                                2 * triangle.a!! * triangle.b!! * cos(Math.toRadians(triangle.gamma!!)))
                        triangle.alpha = Math.toDegrees((acos((triangle.a!!.pow(2) -
                                triangle.b!!.pow(2) -
                                triangle.c!!.pow(2)) / (-2 * triangle.b!! * triangle.c!!))))
                        triangle.beta = 180 - triangle.alpha!! - triangle.gamma!!
                    }
                }
            }
        }

        return triangle
    }

    fun createResStr(context: Context): String {
        return if (check()) {
            context.getString(R.string.result, a, b, c, alpha, beta, gamma, area(), mode)
        } else {
            context.getString(R.string.result_err,
                context.getString(R.string.result, a, b, c, alpha, beta, gamma, area(), mode))
        }
    }

    fun area(): Double {
        return (b!! * sin(Math.toRadians(alpha!!)) * c!!) / 2
    }

    fun countAngles(): Int {
        var count = 0
        if (alpha != null) count += 1
        if (beta != null) count += 1
        if (gamma != null) count += 1
        return count
    }

    fun countEdges(): Int {
        var count = 0
        if (a != null) count += 1
        if (b != null) count += 1
        if (c != null) count += 1
        return count
    }

    private fun check(): Boolean {
        if (a == null ||
                b == null ||
                c == null ||
                alpha == null ||
                beta == null ||
                gamma == null) {
            return false
        }
        if ((alpha!! + beta!! + gamma!!) != 180.0) {
            return false
        }
        return true
    }
}
