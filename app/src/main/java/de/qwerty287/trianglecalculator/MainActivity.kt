package de.qwerty287.trianglecalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import androidx.core.widget.doAfterTextChanged

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val p = Parser(Triangle(null, null, null, null, null, null))
        val a: EditText = findViewById(R.id.a)
        val b: EditText = findViewById(R.id.b)
        val c: EditText = findViewById(R.id.c)
        val aa: EditText = findViewById(R.id.aa)
        val ba: EditText = findViewById(R.id.ba)
        val ga: EditText = findViewById(R.id.ga)
        val res: TextView = findViewById(R.id.res)

        a.doAfterTextChanged {
            if (it != null) {
                val value: Double? = try {
                    it.toString().toDouble()
                } catch (e: Exception) {
                    null
                }
                p.set(p.triangle.apply {
                    this.a = value
                })
            }
            p.parse(res)
        }

        b.doAfterTextChanged {
            if (it != null) {
                val value: Double? = try {
                    it.toString().toDouble()
                } catch (e: Exception) {
                    null
                }
                p.set(p.triangle.apply {
                    this.b = value
                })
            }
            p.parse(res)
        }

        c.doAfterTextChanged {
            if (it != null) {
                val value: Double? = try {
                    it.toString().toDouble()
                } catch (e: Exception) {
                    null
                }
                p.set(p.triangle.apply {
                    this.c = value
                })
            }
            p.parse(res)
        }

        aa.doAfterTextChanged {
            if (it != null) {
                val value: Double? = try {
                    it.toString().toDouble()
                } catch (e: Exception) {
                    null
                }
                p.set(p.triangle.apply {
                    alpha = value
                })
            }
            p.parse(res)
        }

        ba.doAfterTextChanged {
            if (it != null) {
                val value: Double? = try {
                    it.toString().toDouble()
                } catch (e: Exception) {
                    null
                }
                p.set(p.triangle.apply {
                    beta = value
                })
            }
            p.parse(res)
        }

        ga.doAfterTextChanged {
            if (it != null) {
                val value: Double? = try {
                    it.toString().toDouble()
                } catch (e: Exception) {
                    null
                }
                p.set(p.triangle.apply {
                    gamma = value
                })
            }
            p.parse(res)
        }
    }
}