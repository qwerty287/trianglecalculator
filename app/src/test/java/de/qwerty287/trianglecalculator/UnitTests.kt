package de.qwerty287.trianglecalculator

import org.junit.Test

import org.junit.Assert.*
import kotlin.math.pow
import kotlin.math.roundToInt
import kotlin.math.sqrt

class UnitTests {
    @Test
    fun testTriangleCalculations() {
        // we need to round some values to prevent issues with numbers with .00000000000001 or similar
        val t = Triangle(30.0, 30.0, 30.0, null, null, null).calcRemainingParts()
        assertEquals(t.countAngles(), 3)
        assertEquals(t.a, 30.0)
        assertEquals(t.gamma!!.roundToInt(), 60)
        assertEquals(t.area().roundToInt(), 390)

        val t2 = Triangle(50.0, 50.0, null, null, null, 90.0).calcRemainingParts()
        assertEquals(t2.c, sqrt(50.0.pow(2) * 2))
        assertEquals(t2.alpha!!.roundToInt(), 45)
        assertEquals(t2.area().roundToInt(), 1250)
    }
}